<?php
$name = "Имя";
$email = "name@mail.ua";
$login = "Логин";
$password = "Пароль";
$language = [
        'RU' => 'Русский',
        'UA' => 'Украинский',
        'EN' => 'Английский',
        'FR' => 'Французский',
    ];
?>

<form action="reg.php" method="get">
    <label>Введите Ваше имя
        <input name="name" type="text" value="<?php echo $name ?>">
    </label><br>
    <label>Введите Ваш e-mail
        <input name="e-mail" type="email" value="<?php echo $email ?>">
    </label><br>
    <label>Введите логин
        <input name="login" type="text" value="<?php echo $login ?>">
    </label><br>
    <label>Введите пароль
        <input name="password" type="password" value="<?php echo $password ?>">
    </label><br>
    <label>Выберите язык
        <select>
            <?php foreach($language as $lang => $value) {
                echo "<option>"."$lang: $value"."</option>";
            }
        ?>
        </select>
    </label><br>
    <input type="submit" value="Отправить">
</form>
