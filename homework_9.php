<?php

$users = [];

$users["17"] = ["name" => "Sergey", "email" => "sergey@gmail.com", "lang" => "ru"];
$users["32"] = ["name" => "Pavel", "email" => "pavel@yandex.ua", "lang" => "en"];
$users["58"] = ["name" => "Anton", "email" => "anton@meta.ua", "lang" => "ua"];
$users["4"] = ["name" => "Alexey", "email" => "alexey@gmail.com", "lang" => "fr"];
$users["39"] = ["name" => "Roman", "email" => "roman@gmail.com", "lang" => "en"];
$users["21"] = ["name" => "Fedor", "email" => "Fedor@test.com", "lang" => "de"];
$users["7"] = ["name" => "Egor", "email" => "egor@test.com", "lang" => "ua"];
$users["18"] = ["name" => "Sergey", "email" => "sergey232@gmail.com", "lang" => "ua"];
$users["37"] = ["name" => "Pavel", "email" => "pavel123@yandex.ua", "lang" => "en"];
$users["68"] = ["name" => "Peter", "email" => "pet@gmail.com", "lang" => "de"];
$users["41"] = ["name" => "Alexey", "email" => "alexey@gmail.com", "lang" => "fr"];
$users["59"] = ["name" => "Andrew", "email" => "an@gmail.com", "lang" => "en"];
$users["26"] = ["name" => "Fedor", "email" => "Fedor@test.com", "lang" => "de"];
$users["8"] = ["name" => "Egor", "email" => "egor123@test.com", "lang" => "fr"];

echo "Первоначальный массив";
echo "<pre>";
    print_r($users);
echo "</pre>";

//Выделяем массив имен

$i = 0;
foreach ($users as $key => $value) {
    $name[$i] = $value["name"];
    $i++;
}

echo "<pre>";
    print_r($name);
echo "</pre>";

// Определяем количество повторений имен

$j = 0;
$count = 0;

for ($i=0; $i < count($name); $i++) {
    $j = $i;
    while ($j<=count($name)) {
            if ($name[$j] == $name[$i]) {
                $count++;
            }
            $j++;
    }

        if ($count>1) {
            echo "Имя ".$name[$i]." повторяется ".$count;
            if ($count>=2 && $count <5) {
                echo " раза<br>";
            } else {
                echo " раз<br>";
                }
        }
            $count = 0;
}

//Создаем массивы языков

$ru = [];
$ua = [];
$en = [];
$fr = [];
$de = [];

foreach ($users as $key => $value) {
    switch ($value["lang"]) {
        case "ru":
            array_push($ru,$value);
            break;
        case "ua":
            array_push($ua,$value);
            break;
        case "fr":
            array_push($fr,$value);
            break;
        case "en":
            array_push($en,$value);
            break;
        case "de":
            array_push($de,$value);
            break;
    }
}

echo "<pre>";
print_r($ua);
echo "</pre>";

echo "<pre>";
print_r($ru);
echo "</pre>";

echo "<pre>";
print_r($en);
echo "</pre>";

echo "<pre>";
print_r($fr);
echo "</pre>";

echo "<pre>";
print_r($de);
echo "</pre>";

//Создаем обратный массив

$new_key = [];
$i = 0;
foreach ($users as $k => $v) {
    $new_key[$i] = $k;
    $i++;
}

$new_key = array_reverse($new_key);
$new = array_reverse($users);
$new_val = array_combine($new_key,$new);

echo "Обратный массив";
echo "<pre>";
print_r($new_val);
echo "</pre>";

//------------------------------------------------------------------------------------
//Решение данных заданий на уроке:

//Имеется такой массив:

$users = [];
$users["23"] = ["name" => "Veronika","surname"=>"Dorofeeva", "email" => "Veronika_Dorofeeva@gmail.com", "lang"=>"ru"];
$users["76"] = ["name" => "Diana","surname"=>"Korshunova", "email" => "Diana_Korshunova@gmail.com", "lang"=>"fr"];
$users["30"] = ["name" => "Varvara","surname"=>"Kozlova", "email" => "Varvara_Kozlova@gmail.com", "lang"=>"ru"];
$users["98"] = ["name" => "Fedor","surname"=>"Osipov", "email" => "Fedor_Osipov@gmail.com", "lang"=>"uk"];
$users["4"] = ["name" => "Veronika","surname"=>"Konovalova", "email" => "Veronika_Konovalova@gmail.com", "lang"=>"en"];
$users["10"] = ["name" => "Fedor","surname"=>"Kozlov", "email" => "Fedor_Kozlov@gmail.com", "lang"=>"uk"];
$users["35"] = ["name" => "Veronika","surname"=>"Korshunova", "email" => "Veronika_Korshunova@gmail.com", "lang"=>"en"];

// В поисках количества повторяющихся имен

$names = [];
foreach ($users as $user) {
    if (empty($names[$user['name']])) {
        $names[$user['name']] = 1; /// $names['Veronika'] = 1;
    } else {
        $names[$user['name']] += 1;
    }
}

foreach ($names as $name => $cnt) {
    if ($cnt > 1) {
        //echo $name . " " . $cnt . "<br>";
    }
}

//Самый быстрый и короткий вариант решения
$names = array_count_values(array_column($users, 'name'));
foreach ($names as $name => $cnt) {
    if ($cnt > 1) {
        //echo $name . " " . $cnt . "<br>";
    }
}



$langs = [];
foreach ($users as $id => $user) {
    $langs[$user['lang']][$id] = $user;
}
echo "<pre>";
//print_r($langs);
echo "</pre>";

//Для создания массива языков
foreach ($users as $id => $user) {
    ${$user['lang']}[$id] = $user; // $ru =
}

$langs = array_unique(array_column($users, 'lang'));
foreach ($langs as $lang) {
    echo "<pre>";
    //print_r($$lang);
    echo "</pre>";
}

$userIds = array_reverse(array_keys($users));
$reversedUsers = [];
foreach ($userIds as $id) {
    $reversedUsers[$id] = $users[$id];
}
$reversedUsers = [];
for(end($users); current($users); prev($users)){
    $reversedUsers[key($users)] = current($users);
}
echo "<pre>";
//print_r($reversedUsers);
echo "</pre>";
