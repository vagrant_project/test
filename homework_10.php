<?php

echo "Преобразование даты"."<br>";
$date = '31-12-2020';
echo $date." => ";
$date_arr = array_reverse(explode("-",trim($date)));
$date_reverse = implode(".",$date_arr);
echo $date_reverse."<br>";

echo "Преобразование первых символов слов в строке<br>";
$str = "london is the capital of great britain";
echo $str." => ";
echo ucwords($str)."<br>";

echo "Определение длины пароля"."<br>";
$n = rand(1,20);
$password = "";
for ($i=1; $i<=$n; $i++) {
    $s = rand(32,126);
    $password .= chr($s);
}

$p = iconv_strlen($password);
echo "Пароль: ".$password."<br>".$p."<br>";
if (7<$p && $p<12) {
    echo "Пароль подходит"."<br>";
}
else {
    echo "Пароль не подходит, количество символов должно быть больше 7 и меньше 12"."<br>"."Нужно придумать другой пароль"."<br>";
}

$string = "1a2b3c4b5d6e7f8g9h0";
echo $string."<br>";
$num_str = iconv_strlen($string);
$arr_str = str_split($string);
for ($i = 0; $i<=$num_str-1; $i++) {
    if (ord($arr_str[$i])>=48 && ord($arr_str[$i])<=57) {
       unset($arr_str[$i]);
    }
}
$new_string = implode("",$arr_str);
echo $new_string;