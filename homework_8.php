<?php
/*
 * Добавить в массив пользователей с предыдущего задания новый параметр lang c возможными значениями:
 *  ru, en, ua, fr, de. С помощью условий проверить, что если первый и последний пользователь имеют
 *  одинаковый язык, вывести приветствие на экран на этом языке, если пользователи имеют разные языки,
 *  то вывести два приветствия на соотвествующих языках.
 */

$users =[];

$users["17"] = ["name" => "Sergey", "email" => "sergey@gmail.com", "lang" => "ru"];
$users["32"] = ["name" => "Pavel", "email" => "pavel@yandex.ua", "lang" => "en"];
$users["58"] = ["name" => "Anton", "email" => "anton@meta.ua", "lang" => "ua"];
$users["4"] = ["name" => "Alexey", "email" => "alexey@gmail.com", "lang" => "fr"];
$users["39"] = ["name" => "Roman", "email" => "roman@gmail.com", "lang" => "en"];
$users["21"] = ["name" => "Fedor", "email" => "Fedor@test.com", "lang" => "de"];
$users["7"] = ["name" => "Egor", "email" => "egor@test.com", "lang" => "ua"];

echo "Первоначальный массив";
echo "<pre>";
print_r($users);
echo "</pre>";

$greeting = ["ru" => "Привет!", "ua" => "Привiт!", "en" => "Hello!", "fr" => "Salut!", "de" => "Hallo!"];

ksort($users);

echo "Отсортированный массив";
echo "<pre>";
print_r($users);
echo "</pre>";

$user_first_lang = $users[array_key_first($users)]["lang"];
$user_last_lang = $users[array_key_last($users)]["lang"];

if ($user_first_lang == $user_last_lang) {
    echo $greeting[$user_first_lang];
}
else {
    echo $greeting[$user_first_lang]."<br>";
    echo $greeting[$user_last_lang];
}