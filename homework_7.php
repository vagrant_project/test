<?php

$users = [];

$users["17"] = ["name" => "Sergey", "email" => "sergey@gmail.com"];
$users["32"] = ["name" => "Pavel", "email" => "pavel@yandex.ua"];
$users["58"] = ["name" => "Anton", "email" => "anton@meta.ua"];
$users["4"] = ["name" => "Alexey", "email" => "alexey@gmail.com"];
$users["39"] = ["name" => "Roman", "email" => "roman@gmail.com"];
$users["21"] = ["name" => "Fedor", "email" => "Fedor@test.com"];
$users["7"] = ["name" => "Egor", "email" => "egor@test.com"];

echo "Первоначальный массив";
echo "<pre>";
print_r($users);
echo "</pre>";

echo "На сайте общее кол-во пользователей ".count($users)."<br>";

krsort($users);

echo "Пользователи в порядке убывания: <br>";
echo "<pre>";
    print_r($users);
echo "</pre>";

$userMaxId = current($users);
$userPostMinId = next($users);
$userMinId = end($users);
$userPredMaxId = prev($users);
    echo "Пользователь с максимальным Id "."<pre>".print_r($userMaxId)."</pre>"."<br>";
    echo "Пользователь с минимальным Id "."<pre>".print_r($userMinId)."</pre>"."<br>";
    echo "Пользователь с предыдущим максимальному Id "."<pre>".print_r($userPredMaxId)."</pre>"."<br>";
    echo "Пользователь с следующим за минимальным Id "."<pre>".print_r($userPostMinId)."</pre>"."<br>";

unset($users[array_key_last($users)]);

echo "<pre>";
print_r($users);
echo "</pre>";